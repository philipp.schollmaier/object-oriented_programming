#include "inventory.h"
#include <cmath>
#include <fstream>



// check whether task 9 is fulfilled!
//void Inventory::add_item(int id, std::string name, double price, std::shared_ptr<const computerSpec> spec)
void Inventory::add_item(std::shared_ptr<Item> newItem)
{
    if (_count < Inventory::MAX_SIZE)
    {
        //computer new_item(id, name, price, spec);
        //// only unique objects are allowed to add them in the list
        ////computer found = find_item(new_item); // find_item() returns standard value, if there is nothing equal
        //const computer& found{find_item(new_item)};
        //std::shared_ptr<const computerSpec> found_spec{ found.get_spec() };

        ////if (0 == found.get_id() &&
        ////    "" == found.get_name() &&
        ////    0.0 == found.get_price() &&

        ////    0 == found_spec->get_ram() &&
        ////    computerSpec::Category::ANY == found_spec->get_category() && 
        ////    0 == found_spec->get_screensize())

        if(newItem->get_id() != find_item(*newItem->get_spec()).get_id())  // just checking for identical ID's
        {
            _items[_count] = newItem;
            _count++; //_count = _count +1;
        }
    }
    else //if (_count < 0 || _count > Inventory::MAX_SIZE)
    {
        throw std::out_of_range("Maximum Number of Items were exceeded or the item counter were corruped");
    }
}


// find the closesd match
const Item & Inventory::find_item(const ItemSpec & otherSpec) const
{
    for (size_t i{ 0U }; i < _count; i++)
        if (_items[i]->get_spec()->matches(otherSpec))
            return *_items[i];

    static const Item def{};
    return def;

    //// temporary reference to specification
    //auto query_spec_ptr{ query.get_spec() }; 

    //for (size_t i = 0; i < _count; i++)
    //{
    //    // for integer type property
    //    if (query.get_id() != 0                      // searched id is not equal to 0
    //        && query.get_id() != _items[i].get_id()) // searched id is also not equal to current id [i]
    //        continue;                                // if there is no match move on -> skip to the next iteration

    //    // for string type property
    //    if (query.get_name() != ""
    //       && query.get_name() != _items[i].get_name())
    //        continue;

    //    // for floating type property
    //    constexpr double epsil{ 0.005 };
    //    if (query.get_price() != 0.0
    //        && (epsil < abs(query.get_price() - _items[i].get_price())))
    //        continue;

    //    
    //    // temporary local pointer to item's specification
    //    //auto item_spec_ptr{ _items[i].get_spec() };

    //    //if (!item_spec_ptr)
    //    //    continue;

    //    // allows to leave the notation like before
    //    //const computerSpec & item_spec{ *item_spec_ptr };  // ref for pointer
    //    


    //   // if (!query_spec_ptr)

    //    // if there is no object behind the ponter -> continue
    //      //  continue;  

    //    // Otherwise
    //    //const computerSpec & query_spec{ *query_spec_ptr };


    //    // further CHECK needed || vs &&
    //    if (!_items[i].get_spec() 
    //        || !query.get_spec() 
    //        || !_items[i].get_spec()->matches(*query.get_spec()))
    //        continue;

    //    return _items[i];
    //}
    //return computer{}; // return the 'default' object
}



//computer Inventory::find_item(const computerSpec& query) const
//{
//    for (size_t i{ 0U }; i < _count; i++)
//    {
//        //auto item_spec_ptr{ _items[i].get_spec() };
//
//        //if (!item_spec_ptr)
//        //    continue;
//
//        //// allows to leave the notation like before
//        //const computerspec& item_spec{ *item_spec_ptr };  // ref for pointer
//
//
//        // prevent against non existant pointer and call matches()
//        // also further CHECK needed && vs ||
//        if (!_items[i].get_spec() || !_items[i].get_spec()->matches(query)) 
//            continue;
//
//        return _items[i];
//    }
//    return computer{}; // return the 'default' object
//}



void Inventory::save(const std::string& csv_file_name) const
{
    std::ofstream os(csv_file_name);

    for (int i = 0; i < get_count(); i++)
    {
        os << *_items[i] << "\n";
    }
    os.close();
    std::cout << "Saved...\n";
}



// according to tasknote not asked to provide loeading from streams via classes Item /ItemSpec
//void Inventory::load(const std::string& csv_file_name)
//{
//    std::ifstream is(csv_file_name); //, ios::in
//    if (!is)  // if inventory.csv is not available exception will be thrown
//    {
//        throw std::logic_error("Data not found!"); 
//    }
//    //computer c{};
//    Item c{};
//    while (is)
//    {
//        //is >> c;
//       // add_item(c.get_id(), c.get_name(), c.get_price(), c.get_spec());
//    }
//    is.close();
//    std::cout << "Loaded...\n";
//}
