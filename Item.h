#pragma once
#ifndef ITEM


#include "ItemSpec.h"
#include <ostream>
#include <memory>


class Item
{
public:
	Item() = default; // not mandatory but necessary for next assignment
	Item(int id, std::shared_ptr<const ItemSpec> spec)
		: _id{ id }
	{
		set_spec(spec);
	}

	int get_id() const
	{
		return _id;
	}

	std::shared_ptr<const ItemSpec> get_spec() const
	{
		return _spec;
	}

	virtual ~Item() = default;

	virtual void send_to(std::ostream& os) const
	{
		os << _id;// ';'; // << std::endl;

		if (_spec)
		{
			os << ';';
			_spec->send_to(os);
			os << ';';
		}

	}

protected:
	// for changing the specifications only in this and in derived classes:
	void set_spec(std::shared_ptr <const ItemSpec> spec)
	{
		_spec = spec;
	}

private:
	int _id;
	std::shared_ptr<const ItemSpec> _spec;

};

std::ostream& operator<<(std::ostream& os, const Item& item);

#endif