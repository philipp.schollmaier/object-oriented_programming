#ifndef COMPUTERSPEC
#define COMPUTERSPEC

#include "ItemSpec.h"
#include <cstddef> // for std::size_t
#include <iostream>


using namespace std;


class computerSpec : public ItemSpec  // inheritated
{
public:
	enum class Category { ANY, LAPTOP, DESKTOP, WORKSTATION, UNDEFINED };

	computerSpec() = default;
	computerSpec(unsigned ram, Category category, unsigned sceensize)
		: _ram { ram }
		, _category { category }
		, _screensize { sceensize }
		{ }

	unsigned get_ram() const { return _ram; }
	Category get_category() const { return _category; }
	unsigned get_screensize() const { return _screensize; }

	bool matches(const ItemSpec& otherSpec) const override; // override virtual functions of ItemSpec

	void send_to(std::ostream& os) const override; // override virtual functions of ItemSpec
	void recv_from(std::istream& is);
	friend std::istream & operator>>(std::istream& is, computerSpec& spec);

private:
	unsigned _ram{ 0 };
	Category _category{ Category::ANY};
	unsigned _screensize{ 0 }; // if available Size in inch, otherwise 0 (=standard value)
};



std::ostream& operator<<(std::ostream& os, computerSpec::Category& category);
std::istream& operator>>(std::istream& is, computerSpec::Category& category);

//std::ostream& operator<<(std::ostream& os, const computerSpec & spec);


#endif