#pragma once

#include "Item.h"
#include "smartphoneSpec.h"
#include <string>

class smartphone : public Item
{
public:
	smartphone() = default;
	smartphone(int id, double price, std::shared_ptr<const smartphoneSpec> spec)
		: Item(id, spec), _price(price) {}

	double get_price() const { return _price; }

	void send_to(std::ostream& os) const override;

private:
	double _price;
};

