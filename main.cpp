// #include "computer.h" // multiple declarations
#include "inventory.h"
#include "computer.h"
#include "smartphone.h"

#include <assert.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <memory>

using namespace std;

// output abstraction object properties to the console window
void show(const Item & item) // no class member function => global => so no const for the function
{

    cout << item << "\n";

    ////const computerSpec& specs{ item.get_spec() };
    //auto specs_ptr{ item.get_spec() };

    //// also directly here possible ##
    //static const char* CATEGORY_S[]{ "Any", "Laptop", "Desktop", "Workstation", "Undefined" };


    //std::cout << "ID: " << item.get_id() << "; "
    //    << "Name: '" << item.get_name() << "'; "
    //    << "Price[$]: " << std::fixed << std::setprecision(2) << item.get_price() << "; ";


    //if (specs_ptr) // check wether there is a real object where the pointer points to
    //{
    //    std::cout << " Category: " << CATEGORY_S[(size_t)specs_ptr->get_category()] << "; " // ##
    //           // << " " << item.operator std::string() << " "  
    //        << "RAM[MB]: " << specs_ptr->get_ram() << "; "
    //        << "Screensize[Inch]: " << specs_ptr->get_screensize() << "; "
    //        << endl;
    //}
}


Item max_price_computer(const Inventory & inv) // of computer
{
    int max_index = 0; // if no entry available -> returning standard price
    double temp_price = { 0.0 };
    double current_price = { 0.0 };
    constexpr double epsil{ 0.005 };

    for (int i = 0; i < inv.get_count(); i++)
    {
        auto& it = inv.get_item(i);
        //  maybe "computer" check over here instead or in addition to nullptr check
        const computer* c = dynamic_cast<const computer*>(&it);
        if (c) // if it is not a computer ->NULLPTR
        current_price = c->get_price();

        if (temp_price < current_price && epsil < abs(temp_price - current_price))  
        {
                max_index = i;
                temp_price = current_price;
        }
    }
    // returns the first maximum object (careful if there is another item with the same price)
    return inv.get_item(max_index); 
}



double avg_price_computer(const Inventory& inv)
{
    double sum_price = { 0.0 };
    int item_count = 0;// inv.get_count();
    for (int i = 0; i < inv.get_count(); i++)
    {
        //  maybe "computer" check over here instead or in addition to nullptr check
        const computer* c = dynamic_cast<const computer*>(&inv.get_item(i));
        if (c)
        {
            // if it is not a computer ->NULLPTR
            sum_price += c->get_price();
            item_count += 1;
        }
    }
    if (item_count <= 0)
    {
        throw std::out_of_range("The item is zero or has a negative value!");
    }
    return sum_price / item_count;
}



int main()
{    
    try {
        Inventory inv; // default constructor is called

        auto spec_computer{ std::make_shared<computerSpec>(8192, computerSpec::Category::DESKTOP, 24) };
        auto c0{ std::make_shared<computer>(1, "Alienware Aurora Ryzen Edition", 1999.99, spec_computer) };
        inv.add_item(c0);
        //assert(inv.get_count() == 1);

        //// Demonstrate implemented algorithm for getting the max value
        //std::cout << "The most expensive listed Computer: \n";
        //show(max_price(inv));


        // seperately spec construction
        auto spec_laptop{ std::make_shared<computerSpec>(4096, computerSpec::Category::LAPTOP, 15) };
        auto c1{ std::make_shared<computer>(2, "Lenovo ThinkPad X1 Carbon", 1799.99, spec_laptop) };
        inv.add_item(c1);
        //assert(inv.get_count() == 2);

        // spec is constructed during passing the argument
        auto c2{ std::make_shared<computer>(3, "Dell Precision", 3799.99,
            std::make_shared<computerSpec>(32768, computerSpec::Category::LAPTOP, 15) )};
        inv.add_item(c2);
        //assert(inv.get_count() == 3);

        // constructing and using shared specs
        auto mac_spec{ std::make_shared<computerSpec>(16384, computerSpec::Category::LAPTOP, 13) };
        auto c3{ std::make_shared<computer>(4, "Apple MacBook Air 13.3 Inch 2020", 1799.99, mac_spec) };
        inv.add_item(c3);
        //assert(inv.get_count() == 4);
        auto c4{ std::make_shared<computer>(5, "Apple MacBook Pro 13 Inch 2020", 2549.99, mac_spec) };
        inv.add_item(c4);
        //assert(inv.get_count() == 5);




        // query
        show(inv.find_item(*mac_spec)); // shows first item of the list that fits the mac_spec criteria
        show(inv.find_item(computerSpec{})); // first item of the list with any value

        // query non-matching criteria
        show(inv.find_item(computerSpec{ 90'000, computerSpec::Category::WORKSTATION, 98 } ));


        // another type of abstraction
        auto s0{ std::make_shared<smartphone>(6, 699.99, std::make_shared<smartphoneSpec>("iPhone SE 2020", smartphoneSpec::Category::IOS)) };
        inv.add_item(s0);

        show(inv.find_item(smartphoneSpec("", smartphoneSpec::Category::IOS)));




        // Demonstrate Saving of Inventory to a file
        auto inv_file_name{ "inventory.csv" };

        cout << "\n"
            << "List of data to save: " << "\n";
        for (int i = 0; i < inv.get_count(); i++)
        {
            show(inv.get_item(i));
        }
        inv.save(inv_file_name);



        cout << "\n" << *c1 << "\n";
        auto filename{ "c1.csv" };
        ofstream ofs(filename);
        if (ofs)
            ofs << *c1 << "\n";
        ofs.close();
        cout <<  "Saved to file...\n";

        // Demonstrate implemented algorithm for getting the max value
        std::cout << "\n" << "The most expensive listed Computer: \n";

        show(max_price_computer(inv));

        // Demonstrate implemented algorithm for getting the avg value
        std::cout << "\nThe average price of all listed computers is: \n"
            << avg_price_computer(inv)
            << "\n"
            << "\n";

        std::cout << "\nItem Count: \n"
            << inv.get_count()
            << "\n"
            << "\n";

        //// Demonstrate Loading of Inventroy from a file into a DIFFERENT inventory instance
        //Inventory inv2;
        //cout << "\n";
        //inv2.load(inv_file_name);
        //cout << "List of loaded data: " << "\n";


        //for (int i = 0; i < inv2.get_count(); i++)
        //{
        //    show(inv2.get_item(i));
        //}



        /*

        // Calling: computer find_item(const computer& query) const;
        computer qry_cmptr{ 0, "Apple MacBook Pro 16 Inch 2020", 0.0,  std::make_shared<computerSpec>() }; // expectation: return the Apple MacBook Pro 16 Inch 2020
        //assert(inv.find_item(qry).get_id() == 4); // id of lenovo it 4
        show(inv.find_item(qry_cmptr));


        // Calling: computer find_item(const computerSpec& spec_query) const; 
        show(inv.find_item(computerSpec{ 8192, computerSpec::Category::LAPTOP, 15 }));
        //assert(inv.find_item(qry_spec).get_category() == computer::Category::LAPTOP);
    
    
        // Assignment 4 A13
        //computerSpec qry_spec1{ 65536, computerSpec::Category::WORKSTATION, 0 };
        auto qry_spec1{ std::make_shared<computerSpec>(65536, computerSpec::Category::WORKSTATION, 0) };
        computer qry_cmptr1{ 0, "", 0.0, qry_spec1 };

        show(inv.find_item(qry_cmptr1)); // calls the first overloded function of find_item(const computer...)
        show(inv.find_item(*qry_spec1)); // calls the second overloded function of find_item(const computerSpec...)
        //both results should be the same 

        */
    }

    catch (std::out_of_range& oore) {
        cerr << "Error: " << oore.what() << endl;// if inventory.csv is not available or were deleted before loading exception will be thrown
    }
    catch (std::logic_error& loe) {
        cerr << "Error: " << loe.what() << endl;
    }
    catch (...) {
        cerr << "Uncaught exception!" << endl;
    }

    return 0;
}