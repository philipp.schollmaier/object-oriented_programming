#include "computer.h"
//#include <string> // for std::getline() // clearfiy why this in inclusion is necessary


//std::ostream& operator<<(std::ostream& os, const computer& spec)
//{
//	spec.send_to(os);
//	return os;
//}

void computer::send_to(std::ostream& os) const
{

	Item::send_to(os);

	os << _name  << ';'
		<< _price;


	//os << _id << ';'
	//	<< _name << ';'
	//	<< _price;
	//
	//if (_spec)
	//{
	//	os << ';';
	//	_spec->send_to(os);
	//}
	//os << "\n";
}


std::istream& operator>>(std::istream& is, computer& item)
{
	item.recv_from(is);
	return is;
}


void computer::recv_from(std::istream& is)
{
	//if (is)
	//	(is >> _id).ignore();

	if (is)
		std::getline(is >> std::ws, _name, ';');
	if (is)
		(is >> _price).ignore();

	auto temp_spec{ std::make_shared<computerSpec>() };
	temp_spec->recv_from(is);
	//_spec = temp_spec;
	set_spec(temp_spec); // set spec to the value of temp_spec
}


//computer::computer() // init standard values
//{
//	_id = 0;
//	_name = "";
//	_price = 0.0;
//	computerSpec _spec;  // create computerSpec object with default constr.-> just standard values
//}

//computer::computer(int id, string name, double price, computerSpec spec)
//{
//	_id = id;
//	_name = name;
//	_price = price;
//	_spec = spec; // create a computerSpec object with specific values
//}

//void computer::init(int id, string name, const Category category, double price)
//{
//	_id = id;
//	_name = name;
//	_category = category;
//	_price = price;
//};



//computer::operator string() const  // also possible directly in the main right before the cout statement
//{
//	//string representations of computer::category values:
//	static const char* category_s[]{ "any", "laptop", "desktop", "workstation", "undefined" };
//	ostringstream str;
//
//	//str << "category: " << category_s[static_cast<size_t>(_category)] << "\n";
//	str << "category: " << category_s[(size_t)_category] << " ";
//	return str.str();
//}
