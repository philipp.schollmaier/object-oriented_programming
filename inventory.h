#ifndef INVENTORY
#define INVENTORY


#include "ItemSpec.h"
#include "Item.h"
//#include "computer.h"
//#include "computerSpec.h" // is already incuded implicity by computer.h but include it explicitly!
#include <string>
#include <exception>
#include <memory>
#include <iostream>


class Inventory
{
public:
    Inventory() : _count{ 0 } { }; // default constructor

    // Initializes the inventory for storing abstraction objects
   // void init() { _count = 0; } // so-called inline bodies...

    // Returns the number of currently stored abstraction objects
    size_t get_count() const { return _count; }

    const Item& operator[](size_t i) const { return get_item(i); }
    
    // Returns stored abstraction objects by its index or throws an exception if index is invalid
    const Item& get_item(size_t i) const
    {
        if (i < _count) return *_items[i];

        throw std::out_of_range("Invalid index value.");
    }

    // Returns stored abstraction object by its index or default if index is invalid
    //computer get_item(size_t i) const { return (i < _count) ? _items[i] : computer{}; }

    // From passed property values, creates and adds new abstraction object in an array _items
    //void add_item(int id, string name, const computerSpec::Category category, double price, computerSpec spec);
    
    //void add_item(int id, std::string name, double price, std::shared_ptr<const computerSpec> spec);
    void add_item(std::shared_ptr<Item> new_item);


    // Looks for a matching abstraction object and returns the first found or default object
   // computer find_item(const computer & query) const;
    //computer find_item(const computerSpec& spec_query) const;
    const Item& find_item(const ItemSpec& spec_query) const;

    void save(const::std::string& csv_file_name) const;
   // void load(const std::string& csv_file_name);
 

private:
    // The maximum number of abstraction objects that can be stored
    static const size_t MAX_SIZE{ 10 };

    // An array for storing abstraction objects
    std::shared_ptr<Item> _items[Inventory::MAX_SIZE];

    // The number of currently stored abstraction objects in the array _items
    size_t _count;
};

#endif