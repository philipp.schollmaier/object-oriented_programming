#include "smartphoneSpec.h"


bool smartphoneSpec::matches(const ItemSpec& itemSpec) const
{
    if (this == &itemSpec) // if comparing to self
        return true;

    // downcast requ derived spec type using dynamic cast operator
    auto temp{ dynamic_cast<const smartphoneSpec*>(&itemSpec) };
    if (nullptr == temp)
        return false; // itemSpec does not refer to computerClass/derived class instance

    const smartphoneSpec& otherSpec{ *temp };

    if ("" != otherSpec.get_name()
        && this->_name != otherSpec.get_name())
        return false;

    // for enum type property
    if (smartphoneSpec::Category::ANY != otherSpec.get_category()
        && this->_category != otherSpec.get_category())
        return false;

    return true;
}

void smartphoneSpec::send_to(std::ostream& os) const
{
    static const char* Category_cstr[]{ "Any", "Android", "iOS", "Other" };
    os << _name << ';'
        << Category_cstr[static_cast<size_t>(_category)];
}