#include "computerSpec.h"

#include <string> // for std::getline()

#ifdef _MSC_VER // Visual C++
    #define strcasecmp _stricmp // then use _stricmp() fnctn
#else
    #include <strings.h>  // for strcasecmp() fnct in POSIX C++
#endif


static const char* Category_cstr[]{ "Any", "Laptop", "Desktop", "Workstation", "Undefined" };



std::ostream& operator<<(std::ostream& os, computerSpec::Category category)
{
    os << Category_cstr[static_cast<size_t>(category)]; // CHECK demo
    return os;
}



//std::ostream& operator<<(std::ostream& os, const computerSpec & spec)
//{
//    spec.send_to(os);
//    return os;
//}


void computerSpec::send_to(std::ostream& os) const
{
    os << _ram << ';'
        << _category << ';'
        << _screensize;
}


std::istream& operator>>(std::istream& is, computerSpec::Category& category)
{
    if (is)
    {
        std::string tmp;
        std::getline(is, tmp, ';');
    
        if (is)
        {
            bool found{ false };

            for (size_t i{ 0 }; i < sizeof(Category_cstr) / sizeof(Category_cstr[0]); i++)
            {
                if (!strcasecmp(tmp.c_str(), Category_cstr[i]))
                {
                    category = static_cast<computerSpec::Category>(i);
                    found = true;
                    break;
                }
            }

            if (!found)
                category = computerSpec::Category::ANY;
        }
    }
    return is;
}


std::istream& operator>>(std::istream& is, computerSpec& item)
{
    item.recv_from(is);
    return is;
}


void computerSpec::recv_from(std::istream& is)
{
    if (is)
        (is >> _ram).ignore();  
    if (is)
        is >> _category; 
    if (is)
        (is >> _screensize).ignore();
}

bool computerSpec::matches(const ItemSpec& itemSpec) const
{
    if (this == &itemSpec) // if comparing to self
        return true;

    // downcast requ derived spec type using dynamic cast operator
    auto temp{ dynamic_cast<const computerSpec*>(&itemSpec) };
    if (nullptr == temp)
        return false; // itemSpec does not refer to computerClass/derived class instance

    const computerSpec& otherSpec{ *temp };

    if (0 != otherSpec.get_ram()
        && this->_ram != otherSpec.get_ram())
        return false;

    // for enum type property
    if (computerSpec::Category::ANY != otherSpec.get_category()
        && this->_category != otherSpec.get_category())
        return false;

    // for integer type property
    if ( 0 != otherSpec.get_screensize()
        && this->_screensize != otherSpec.get_screensize())
        return false;

	return true;
}

//computerSpec::computerSpec() // init standard values
//{
//	_ram = 0;
//	_category = computerSpec::Category::ANY;
//	_screensize = 0;
//}

// possibility to add more overloaded constructors, if there are for instance just one or two information given

//computerSpec::computerSpec(int ram, computerSpec::Category category, int screensize)
//{
//	_ram = ram;
//	_category = category;
//	_screensize = screensize;
//}