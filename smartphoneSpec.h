#pragma once
#include "ItemSpec.h"
#include <string>

class smartphoneSpec : public ItemSpec
{
public:
	enum class Category {ANY, ANDROID, IOS, OTHER};

	smartphoneSpec() = default;
	smartphoneSpec(const std::string& name, Category category)
		: ItemSpec(), _name(name), _category(category) {}

	bool matches(const ItemSpec& itemSpec) const override;

	std::string get_name() const { return _name;  }
	Category get_category() const { return _category; }

	void send_to(std::ostream& os) const override;
private:
	std::string _name;
	Category _category;

};

