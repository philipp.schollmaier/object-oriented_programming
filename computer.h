#pragma once
#ifndef COMPUTER
#define COMPUTER

#include "Item.h"
#include "computerSpec.h"
#include <string>
#include <memory>
#include <iostream>



using namespace std;

class computer : public Item // inheritated from Item "is a" relationship
{
public:
	//enum class Category { ANY, LAPTOP, DESKTOP, WORKSTATION, UNDEFINED };

	computer() = default; // default constructor
	computer(unsigned id, string name, double price, std::shared_ptr<const computerSpec> spec) // overloaded constructor & using initializer list
			: Item { id, spec } // !
			, _name{ name }
			, _price{ price }
			//, _spec{ spec }
			{ }
																		  

	//unsigned get_id() const { return _id; }

	std::string get_name() const { return _name; }
	double get_price() const { return _price; }
	//computerSpec get_spec() const { return _spec; }
	//std::shared_ptr<const computerSpec> get_spec() const { return _spec; }

	void send_to(std::ostream& os) const override;
	void recv_from(std::istream& is);
	friend std::istream& operator>>(std::istream& is, computer& item);

private:
	//unsigned _id{ 0U };
	std::string _name{""};
	double _price{ 0.0 };
	//computerSpec _spec;
	//std::shared_ptr<const computerSpec> _spec; // const pointer 
};

//std::ostream& operator<<(std::ostream& os, const computer& item); // binary operator overloading

#endif